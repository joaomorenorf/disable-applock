import subprocess
print("Querrying the phone for installed apps")
apps_byte = subprocess.run("adb shell cmd package list packages", capture_output=True).stdout
apps_string = apps_byte.decode("utf-8")
apps_list = apps_string.splitlines()
apps = []
for proc in apps_list:
    if proc[:8] == "package:":
        apps.append(proc[8:])
    else:
        print("ERROR:")
        print(proc)
blocked = []
print('\033[K' + "done")
print("Discovering blocked apps")
for ii, app in enumerate(apps):
    print('\033[K' + "%.2f%%\t" % (100*ii/len(apps)) + app, end="\r")
    tryopen = subprocess.run("adb shell monkey -v -p %s 1" % app, capture_output=True).stdout.decode("utf-8")
    if "No activities found to run" in tryopen:
        pass
    elif "act=miui.intent.action.CHECK_ACCESS_CONTROL pkg=com.miui.securitycenter cmp=com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl" in tryopen:
        blocked.append(app)
    else:
        close_app = subprocess.run("adb shell am force-stop %s" % app, capture_output=True).stdout.decode("utf-8")
print('\033[K' + "100%%\tDONE")
print("Disabling blocked apps")
for ii, app in enumerate(blocked):
    print('\033[K' + "%.2f%%\t" % (100*ii/len(blocked)) + app, end="\r")
    disable_app = subprocess.run("adb shell pm disable-user --user 0 %s" % app, capture_output=True).stdout.decode("utf-8")
print('\033[K' + "100%%\tDONE")
d = ""
while d != "yes":
	d = input("Change the password in your phone, note that it will not warn you about erasing all your files. Aready changed the password? Type yes to continue:\n")
print("Re-enabling blocked apps")
for ii, app in enumerate(blocked):
    print('\033[K' + "%.2f%%\t" % (100*ii/len(blocked)) + app, end="\r")
    disable_app = subprocess.run("adb shell pm enable --user 0 %s" % app, capture_output=True).stdout.decode("utf-8")
print('\033[K' + "100%%\tDONE")
print("Congrats, you are free")
